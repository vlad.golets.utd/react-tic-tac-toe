import './App.css';
import {useState} from "react";

function Board({xIsNext, squareList, onPlay}) {
  function handleClick(index) {
    if (squareList[index] || calculateWinner(squareList)) return;

    const nextSquares = squareList.slice();

    xIsNext
        ? nextSquares[index] = 'X'
        : nextSquares[index] = 'O';
    onPlay(nextSquares, index);
  }

  let status;
  const { winner, winnerSquares } = calculateWinner(squareList) || {};
  const isPossibleSteps = squareList.some(square => square === null);

  status = winner
      ? `Winner: ${winner}`
      : isPossibleSteps
          ? `Next player: ${xIsNext ? 'X' : 'O'}`
          : `There's a draw! No winner.`;

  return (
      <div className="tic-tac-toe__board-wrapper">
        <h2 className="tic-tac-toe__status">
          {status}
        </h2>

        <div className="tic-tac-toe__board">
          {squareList.map((squareValue, index) => {
            return <BoardSquare
                key={index}
                value={squareValue}
                onSquareClick={() => handleClick(index)}
                isWinnerSquare={winnerSquares && winnerSquares.includes(index)}
            />;
          })}
        </div>
      </div>
  );
}

function BoardSquare({value, onSquareClick, isWinnerSquare}) {
  return (
      <button
          className={`tic-tac-toe__square ${isWinnerSquare ? 'tic-tac-toe__square--winner' : ''}`}
          onClick={onSquareClick}
      >
        {value}
      </button>
  );
}

function calculateWinner(squareList) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];

    if (squareList[a] && squareList[a] === squareList[b] && squareList[a] === squareList[c]) {
      return {
        winner: squareList[a],
        winnerSquares: [a, b, c]
      };
    }
  }

  return null;
}

function BoardInfo({history, isSortAscending, currentStep, onJumpTo, onToggleSort}) {
  const sortedHistory = isSortAscending ? history : [...history].reverse();

  return (
      <div className="tic-tac-toe__steps">
        {history.length > 1 ?
            <BoardInfoSort
                isSortAscending={isSortAscending}
                onToggleSort={onToggleSort}
            />
            : ''
        }

        <ol className="tic-tac-toe__steps-list">
          {sortedHistory.map((squares, index) => {
            const sortedIndex = isSortAscending ? index : history.length - 1 - index;

            return <BoardInfoStep
                key={sortedIndex}
                step={sortedIndex}
                currentStep={currentStep}
                onJumpTo={onJumpTo}
                index={squares.index}
            />
          })}
        </ol>
      </div>
  );
}

function BoardInfoStep({step, index, currentStep, onJumpTo}) {
  const row = Math.floor(index / 3) + 1;
  const col = index % 3 + 1;

  const description = step > 0
      ? currentStep === step
          ? `You're at step #${currentStep} (Row - ${row}, Column - ${col})`
          : `Go to step #${step} (Row - ${row}, Column - ${col})`
      : currentStep === step
          ? `You're at the beginning of the game`
          : `Go to the beginning of the game`;

  return (
      <li className="tic-tac-toe__steps-list-item">
        {currentStep !== step
            ? <button className="tic-tac-toe__steps-item tic-tac-toe__steps-item--button" onClick={() => onJumpTo(step)}>{description}</button>
            : <span className="tic-tac-toe__steps-item">{description}</span>
        }
      </li>
  );
}

function BoardInfoSort({onToggleSort, isSortAscending}) {
  return (
      <button className={`tic-tac-toe__steps-sort ${!isSortAscending ? 'tic-tac-toe__steps-sort--toggle' : ''}`} onClick={onToggleSort}>
        Sort {isSortAscending ? 'Descending' : 'Ascending'}

        <svg
            fill="#000000"
            height="12"
            width="12"
            version="1.1"
            id="Layer_1"
            viewBox="0 0 407.437 407.437"
        >
          <polygon points="386.258,91.567 203.718,273.512 21.179,91.567 0,112.815 203.718,315.87 407.437,112.815 "/>
        </svg>
      </button>
  );
}

export default function TicTacToe() {
  const [history, setHistory] = useState([
    {
      squares: Array(9).fill(null),
      index: null
    }
  ]);
  const [currentStep, setCurrentStep] = useState(0);
  const xIsNext = currentStep % 2 === 0;
  const currentSquares = history[currentStep].squares;
  const [isSortAscending, setIsSortAscending] = useState(true);

  function handlePlay(nextSquares, index) {
    const nextHistory = [...history.slice(0, currentStep + 1), { squares: nextSquares, index }];
    setHistory(nextHistory);
    setCurrentStep(nextHistory.length - 1);
  }

  function handleJumpTo(index) {
    setCurrentStep(index);
  }

  function handleSort() {
    setIsSortAscending(!isSortAscending);
  }

  return (
      <div className="tic-tac-toe">
        <Board
            xIsNext={xIsNext}
            squareList={currentSquares}
            onPlay={handlePlay}
        />

        <BoardInfo
            history={history}
            isSortAscending={isSortAscending}
            currentStep={currentStep}
            onJumpTo={handleJumpTo}
            onToggleSort={handleSort}
        />
      </div>
  );
}
